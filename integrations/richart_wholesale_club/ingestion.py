import os,sys
import numpy as np
import pandas as pd
import re
from sqlalchemy.orm import sessionmaker
from sqlalchemy.types import Integer, Text, String, DateTime
from sqlalchemy import create_engine

sys.path.append("...")
from prueba.models import BranchProduct, Product, Base

#PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
#ASSETS_DIR = os.path.join(PROJECT_DIR, "assets")
#PRODUCTS_PATH = os.path.join(ASSETS_DIR, "PRODUCTS.csv")
#PRICES_STOCK_PATH = os.path.join(ASSETS_DIR, "PRICES-STOCK.csv")
path = r'E:\virtualEnt\anotherVirtual\prueba\assets'
PRODUCTS_PATH = os.path.join(path,'PRODUCTS.csv')
PRICES_STOCK_PATH = os.path.join(path,'PRICES-STOCK.csv')

def is_unit(u,s = 0):
    if '"' in u:
        if '"""' in u:
            return None
        if '"' in u:
            return None
        else:
            index = u.find('"')
            u = u[0:index]
            u = u + "''"
    pattern = r".*?([\d.]+\s?)?(GRS\.?|TAX|MTS|GAL|G|L|M|%|P|C|PACK|X|EN|\/|\+|HJ|G\/L|ONZ|M|CJA|GR|CC|KG\.?|ML\.?|''|LT\.?|OZ|PZ|CM|MG|UN|AA|PU|UL|PLG)"
    #m = re.match(pattern, u)
    x = re.search(pattern, u)
    if x:
        if s == 0:
            return u.strip()
        else:
            if s.isdigit():
                if not u[0].isdigit():
                    return s.strip() + u.strip()
                else:
                    return u.strip()
            else:
                return u.strip()
    else:
        return None

def send_data(dframe,dframetienda):
    engine = create_engine("sqlite:///db.sqlite")
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    product = (session.query(Product)
            .filter_by(store='Richard', sku=dframe['SKU'])
            .first()
    )
    if product is None:
        product = Product(store=dframe['STORE'], sku=dframe['SKU'])
    product.barcodes = dframe["BARCODES"]
    product.brand = dframe["BRAND"]
    product.name = dframe["NAME"]
    product.description = dframe["DESCRIPTION"]
    product.package = dframe['PACKAGE']
    product.image_url = dframe["IMAGE_URL"]
    product.categort = dframe["NEWCATEGORY"]
    session.add(product)
    session.commit()
    branch_product = (
            session.query(BranchProduct)
            .filter_by(product=product, branch=dframetienda["BRANCH"])
            .first()
        )
    if branch_product is None:
        branch_product = BranchProduct(product=product, branch=dframetienda["BRANCH"])
    branch_product.stock = dframetienda["STOCK"]
    branch_product.price = dframetienda["PRICE"]
    session.add(branch_product)
    session.commit()
    session.close()

def process_csv_files():
    #abriendo los archivos
    products_df = pd.read_csv(filepath_or_buffer=PRODUCTS_PATH, sep="|",)
    prices_stock_df = pd.read_csv(filepath_or_buffer=PRICES_STOCK_PATH, sep="|",)
    prices_stock_df['STOCK'] = prices_stock_df['STOCK'].astype(int)
    #creando un dataframe sólo con las marcas que me interesan y sólo teniendo en cuenta los productos con stock
    brands_importants = prices_stock_df.loc[(prices_stock_df['BRANCH']=='MM') | (prices_stock_df['BRANCH']=='RHSM')]
    brands_importants = brands_importants.loc[brands_importants['STOCK'] > 0]
    brands_importants.reset_index(drop=True,inplace=True) #ordenando un poco
    #eliminando las etiquetas HTML de la columna descriptión
    products_df.DESCRIPTION = products_df.DESCRIPTION.str.replace('<[^<]+?>', '')
    #sólo me interesan los productos de las marcas que separamos anteriormente en el dataframe brands_importants
    products_df2 = pd.merge(products_df,brands_importants,on='SKU')
    #no nos interesa tener SKU duplicados en la base de datos, en la tabla de productos
    products_df2 = products_df2.drop_duplicates(subset=['SKU'],keep='first')
    #uniendo las categorías y las subcategorías en un sólo valor separado por |, y dejandolas en lower case
    products_df2['NEWCATEGORY'] = products_df2['CATEGORY'].astype(str) +'|'+ products_df2['SUB_CATEGORY'].astype(str) +'|'+ products_df2['SUB_SUB_CATEGORY'].astype(str) 
    products_df2['NEWCATEGORY'] = products_df2['NEWCATEGORY'].str.lower()
    #tomando la información para package
    column_of_units = products_df2["DESCRIPTION"].str.split().str[-2:]
    new_serie = []
    for index, value in column_of_units.items():    
        if len(value) > 1:
            new_serie.append(is_unit(value[1],value[0]))
        elif len(value) == 1:
            new_serie.append(is_unit(value[0]))
        else:
            print(value)
            new_serie.append(None)                  
            
    column_of_units = pd.Series(new_serie)
    products_df2['PACKAGE'] = column_of_units.to_frame()
    products_df2.reset_index(drop=True,inplace=True) #ordenando un poco
    products_df2['STORE'] = 'Richard'
    dframe ={}
    dframetienda = {}
    brands_importants['BRANCH'] = brands_importants['BRANCH'].astype(str)
    brands_importants['SKU'] = brands_importants['SKU'].astype(str)
    products_df2['SKU'] = products_df2['SKU'].astype(str)
    products_df2['DESCRIPTION'] = products_df2['DESCRIPTION'].astype(str)
    products_df2['NAME'] = products_df2['NAME'].astype(str)
    products_df2['IMAGE_URL'] = products_df2['IMAGE_URL'].astype(str)
    products_df2['NEWCATEGORY'] = products_df2['NEWCATEGORY'].astype(str)
    products_df2['PACKAGE'] = products_df2['PACKAGE'].astype(str)
    products_df2['BRAND'] = products_df2['BRAND'].astype(str)
    products_df2['BRANCH'] = products_df2['BRANCH'].astype(str)
    for i, row in products_df2.iterrows():
        dframe = {'PACKAGE':products_df2['PACKAGE'][i], 'NEWCATEGORY':products_df2['NEWCATEGORY'][i],
                   'SKU':products_df2['SKU'][i], 'BARCODES':products_df2['BARCODES'][i], 'BRAND':products_df2['BRAND'][i],
                   'DESCRIPTION':products_df2['DESCRIPTION'][i],'IMAGE_URL':products_df2['IMAGE_URL'][i],'STORE': products_df2['STORE'][i],
                   'NAME':products_df2['NAME'][i]}
        dframetienda ={"BRANCH":brands_importants["BRANCH"][i],"STOCK":brands_importants["STOCK"][i],"PRICE":brands_importants["PRICE"][i]}
        send_data(dframe,dframetienda)
    #products_df2.drop(columns=['B', 'C'])

if __name__ == "__main__":
    #put()
    process_csv_files()
