import sqlite3
from sqlite3 import Error

def create_connection(db):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db)
        return conn
    except Error as e:
        print(e)

    return conn

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)
def main():
    database = "db.sqlite"

    sql_create_products_table = """ CREATE TABLE IF NOT EXISTS products (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        store text NOT NULL,
                                        sku text,
                                        barcodes text,
                                        brand text,
                                        name text NOT NULL,
                                        description text,
                                        package text,
                                        image_url text,
                                        category text,
                                        url text
                                    ); """

    sql_create_branchproducts_table = """CREATE TABLE IF NOT EXISTS branchproducts (
                                    id integer PRIMARY KEY AUTOINCREMENT,
                                    product_id integer NOT NULL,
                                    branch text NOT NULL,
                                    stock integer NOT NULL,
                                    price real NOT NULL,
                                    FOREIGN KEY (product_id) REFERENCES products (id)
                                );"""

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_products_table)

        # create tasks table
        create_table(conn, sql_create_branchproducts_table)
    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()